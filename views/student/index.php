<?php

use yii\helpers\Html;

?>
<div class="site-about">
    <h1>Single Record without Iteration Index Action</h1>

    <p>
    	<b>Name:</b> <?= $student->name ?>
    	<br />
    	<b>ID:</b><?= $student->id?>
    	<br />
    	<b>Age:</b><?= $student->age ?>
    	<br />    	
    </p>
</div>