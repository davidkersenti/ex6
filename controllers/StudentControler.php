<?php
	namespace app\controllers;

	use Yii;
	use app\models\Student;
	use yii\web\Controller;
	use yii\db\ActiveRecord;

	class StudentController extends Controller
	{		
	    public function actionIndex()
	    {
	    	$student = Student::find()			    
			    ->one();
	        return $this->render('index', ['student' => $student]);
	    }

	    public function actionShow()
	    {
	    	$student = Student::getAll();
	        return $this->render('show', ['student' => $student]);
	    }
	}
?>