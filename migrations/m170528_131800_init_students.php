<?php

use yii\db\Migration;

class m170528_131800_init_students extends Migration
{
  public function up()
    {
		$this->createTable(
            'students',
            [
                'id' => 'pk',
                'name' => 'string',
                'email' => 'string',
				'age' => 'integer',
               
            ],
            'ENGINE=InnoDB'
        );
    }
    public function down()
    {
        echo "m170528_131800_init_students cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
