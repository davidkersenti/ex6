<?php

use yii\db\Migration;

class m170518_145857_init_customers extends Migration
{
       public function up()
    {
		$this->createTable(
            'customers',
            [
                'id' => 'pk',
                'name' => 'string',
                'email' => 'string',
               
            ],
            'ENGINE=InnoDB'
        );
    }
    public function down()
    {
        $this->dropTable('customers');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
