<?php
	namespace app\models;

	use yii\db\ActiveRecord;

	class Student extends ActiveRecord
	{
		public static function tableName()
		{
			return 'students';
		}

		public static function getAll()
		{
			$s = Student::find()			    
			    ->orderBy('id')
			    ->all();

			return $s;    
		}
	}
?>